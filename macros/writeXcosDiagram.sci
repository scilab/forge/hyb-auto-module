// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//writeXcosDiagram(fn, dgr) - Write diagram dgr (scs_m structure) to .xcos file named fn
//returns boolean, %f operation failed
//This function can be called without arguments, in this case it returns boolean - 
//whether a Cpp and Java functions required for writing of .xcos files are available
function r=writeXcosDiagram(fn, dgr)
if argn(2)<>0 & argn(2)<>2 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d or %d expected.\n"),"writeXcosDiagram",0,2))
end
if argn(2)==0 then
	r=%t
	if execstr("hybm_xcosHDF5ToDiagram("""","""",%f)","errcatch")<>0 then
		r=%f
	end
	return
end
if type(fn)<>10 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"writeXcosDiagram",1));
end
if size(fn,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string expected.\n"),"writeXcosDiagram",1));
end
if type(dgr)<>17 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: scs_m structure expected.\n"),"writeXcosDiagram",2));
end
//obtain a free temporary file name (needed util Java H5 library locks files)
h5File=TMPDIR+filesep()+hybm_save_name("_wrxdgx_",".h5")+".h5"
scs_m=dgr
export_to_hdf5(h5File,"scs_m")
hybm_xcosHDF5ToDiagram(h5File,fn,%t)
mdelete(h5File)
r=%t
endfunction
