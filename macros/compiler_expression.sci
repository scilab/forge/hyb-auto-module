//  Scicos
//
//  Copyright (C) INRIA - METALAU Project <scicos@inria.fr>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// See the file ../license.txt
// 

function [ok,%ok1,ipar,rpar,%nz]=compiler_expression(%foo)	
  ok=%t,%ok1=%f,ipar=[],rpar=[],%nz=[]
  if exists('%scicos_context') then
    %mm=getfield(1,%scicos_context)
    for %mi=%mm(3:$)
      if execstr(%mi+'=%scicos_context(%mi)','errcatch')<>0 then
	ok=%f
      end
    end
  end 
  if ok then
    ok=execstr('[%ok1,ipar,rpar,%nz]=compile_expr(%foo)','errcatch')==0
  end
endfunction

function [ok,%ipar,%rpar,%nz]=compile_expr(%foo)
  ok=%t
  %lst=macr2lst(%foo);
  %mm=macrovar(%foo);
  %MM=%mm(3);
  %FF=['sin';'cos';'tan';'exp';'log';
       'sinh';'cosh';'tanh';
       'int';'round';'ceil';'floor';
       'sign';'abs';'max';'min';
      'asin';'acos';'atan';'asinh';'acosh';'atanh';
       'atan2';
       'log10';
      ]; // ops above 100
  %num_arg=[1;1;1;1;1;
	    1;1;1
	    1;1;1;1;
	    1;1;2;2;
	   1;1;1;1;1;1;
	    2;
	    1
	   ];  //number of arguments
  %ZCR=[16,17,18,19,20,21,28,29,30,109,110,111,112,113,114,115,116];  // ops with zero-crossing
  %UU=%mm(1)
  %ipar=[]
  %rpar=[]
  %nrpar=0
  %nz=0
  %ijk=4
  while %ijk<length(%lst)
    %ijk=%ijk+1
    select evstr(%lst(%ijk)(1))
     case 2
      %jjk=find(%lst(%ijk)(2)==%FF)
      if %jjk<> [] then
	if evstr(%lst(%ijk)(4))<>%num_arg(%jjk) then
	  message(%lst(%ijk)(2)+' must have '+string(%num_arg(%jjk))+' arguments')
	  ok=%f
	  return
	else
	  %ipar=[%ipar;5;100+%jjk]
	  if or(100+%jjk==%ZCR) then %nz=%nz+1,end
	  %ijk=%ijk+1
	end
      else
	%jjk=find(%lst(%ijk)(2)==%MM)
	if %jjk<> [] then
	  if ~exists(%MM(%jjk)) then
	    message('Variable '+%MM(%jjk)+' is not defined.')
	    ok=%f
	    return
	  end
	  %var=evstr(%MM(%jjk))
	  if size(%var,'*')<>1 then
	    message('Variable '+%MM(%jjk)+' is not scalar.')
	    ok=%f
	    return
	  else
	    %nrpar=%nrpar+1
	    %rpar(%nrpar)=%var
	    %ipar=[%ipar;6;%nrpar]
	  end
	else
	  %jjk=find(%lst(%ijk)(2)==%UU)
	  if %jjk<> [] then
	    %ipar=[%ipar;2;%jjk]
	  else
	    message('Unknown variable '+%lst(%ijk)(2))
	    ok=%f
	  end
	  //%ipar=[%ipar;2;evstr(strsubst(%lst(%ijk)(2),'u',''))]
	end
      end
     case 5
      // case of - with one operand (-u1)
      if (evstr(%lst(%ijk)(2))==2)&(evstr(%lst(%ijk)(3))==1) then
	%ipar=[%ipar;5;99]
      else
	%ipar=[%ipar;5;evstr(%lst(%ijk)(2))]
	if or(evstr(%lst(%ijk)(2))==%ZCR) then %nz=%nz+1,end
      end
     case 6
      //      %ipar=[%ipar;6;evstr(%lst(%ijk)(2))]
      %nrpar=%nrpar+1
      %rpar(%nrpar)=evstr(%lst(%ijk)(2))
      %ipar=[%ipar;6;%nrpar]
    end
  end
endfunction
