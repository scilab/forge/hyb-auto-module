// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//Function [st_eqls,transls,jcondls,init,y0,ierr]=hybm_parse(dgr) analyses Xcos diagram dgr
//(scs_m structure) which represents a hybrid automaton state diagram and produces
//descriptions of state equations (st_eqls), mode transitions (transls) and
//jump conditions (jcondls) and initial state (init, y0) in the form, acceptible by
//hybm_gen_maindgr function. On error, function displays message and sets ierr=1
function [st_eqls,transls,jcondls,init,y0,insz,ierr]=hybm_parse(dgr)
if argn(2)<>1 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"),"hybm_parse",1))
end
if type(dgr)<>17 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: scs_m structure expected.\n"),"hybm_parse",1));
end
st_eqls=list()
transls=list()
jcondls=list()
init=0
y0=[]
ierr=0
if size(dgr.props.doc)<3 then
	ierr=1
	messagebox("Parse error: malformed state diagram") //parameter fields are absent
	return
end
M=evstr(dgr.props.doc(2)) //dimension of phase space
y0=evstr(dgr.props.doc(3))
if size(dgr.props.doc)>=4 then
	insz=evstr(dgr.props.doc(4)) //input control vector size
else
	insz=0
end
nob=size(dgr.objs)
id=0 //counter for hybrid automaton modes
linkm=[] //vector of diagram links [[from1,to1];[from2;to2];...]
modeids=[] //vector which associates automaton mode with object index: modeids(o)=mode
for i=1:nob //fill init, linkm, modeids
	ob=dgr.objs(i)
	flds=getfield(1,ob)
	if flds(1)=="Link" then
		linkm=[linkm;[ob.from(1),ob.to(1)]] //save links
		continue
	end
	if flds(1)<>"Block" then continue, end //skip Text objects
	if ob.gui<>"CSB_f" & ob.gui<>"DSB_f" then
		if ob.gui=="TGB_f" | ob.gui=="SPLIT_f" then
			if ~checkexpr(ob.graphics.exprs(2),M,insz) then
				ierr=1
				messagebox("Parse error: invalid expression "+ob.graphics.exprs(2))
				return
			end
			continue //skip TGB and splitters
		else
			ierr=1
			messagebox("Parse error: only CSB_f, DSB_f, TGB_f and splitters are allowed in hybrid automaton state diagram")
			return
		end
	end
	id=id+1
	modeids(i)=id
	isinit=int(evstr(ob.graphics.exprs(4)))
	if isinit==1 then
		if init==0 then
			init=id
		else
			ierr=1
			messagebox("Parse error: only one initial state is allowed in hybrid automaton state diagram")
			return
		end
	end
end
if init==0 then
	ierr=1
	messagebox("Parse error: initial state is not specified in hybrid automaton state diagram")
	return
end
splconn=[] //splitter connectivity forest
tgbconn=[] //TGB blocks connectivity
slinkm=[] //matrix of links with CSB/DSB destination
for i=1:size(linkm,1) //fill splconn, tgbconn and slinkm
	src=linkm(i,1)
	dest=linkm(i,2)
	if dgr.objs(dest).gui=="SPLIT_f" then
		splconn(dest)=src
	elseif dgr.objs(dest).gui=="TGB_f" then
		tgbconn(dest)=src
	elseif dgr.objs(dest).gui=="CSB_f" | dgr.objs(dest).gui=="DSB_f"
		slinkm=[slinkm;linkm(i,:)]
	end
end
tlinkm=[] //vector of "transitive" (through splitters and TGB blocks) links between modes
jcm=[] //vector of corresponding jump conditions
for i=1:size(slinkm,1)
	src=slinkm(i,1)
	dest=slinkm(i,2)
	nskiptgb=0 //number of TGB skips
	tgbi=0 //index of skipped TGB
	if dgr.objs(src).gui=="TGB_f" then
		tgbi=src
		src=tgbconn(src) //TGBs can not be chained
		nskiptgb=nskiptgb+1
	end
	while dgr.objs(src).gui=="SPLIT_f" do //skip all splitters
		src=splconn(src)
	end
	if dgr.objs(src).gui=="TGB_f" then
		tgbi=src
		src=tgbconn(src)
		nskiptgb=nskiptgb+1
	end
	if nskiptgb>1 then
		ierr=1
		messagebox("Parse error: transition guard blocks in hybrid automaton state diagrams can not be chained")
		return
	elseif nskiptgb==0 then
		ierr=1
		messagebox("Parse error: state blocks can not be connected without transition guard blocks in hybrid automaton state diagrams")
		return
	end
	if dgr.objs(src).gui<>"CSB_f" & dgr.objs(src).gui<>"DSB_f" then
		ierr=1
		messagebox("Parser error: malformed state diagram") //invalid connections
		return
	end
	eq=dgr.objs(tgbi).graphics.exprs(2) //jump condition
	cross=dgr.objs(tgbi).graphics.exprs(1) //jump cross condition
	if int(evstr(cross))==1 then
		eq="-("+eq+")" //change sign, i.e. transition on "+ to -"
	end
	jcm=[jcm;eq]
	tlinkm=[tlinkm;[modeids(src),modeids(dest)]]
end
id=0
for i=1:nob //fill st_eqls, transls, jcondls
	ob=dgr.objs(i)
	flds=getfield(1,ob)
	if flds(1)<>"Block" then continue, end
	if ob.gui=="CSB_f" | ob.gui=="DSB_f" then
		id=id+1
		eq=ob.graphics.exprs(3) //RHS of differential/difference equation
		eqs=evstr(eq) //evaluate to vector of strings
		if size(eqs,1)<>M | size(eqs,2)<>1 then
			ierr=1
			messagebox("Parse error: the following vector of state equations has invalid size "+eq)
			return
		end
		for no=1:M //check equations
			if ~checkexpr(eqs(no),M,insz) then
				ierr=1
				messagebox("Parse error: invalid expression "+eqs(no))
				return
			end
		end
		if ob.gui=="CSB_f" then
			st_eqls(id)=list(eqs,"c")
		else
			tdelta=evstr(ob.graphics.exprs(5)) //Time delta
			st_eqls(id)=list(eqs,"d",tdelta)
		end
		inds=find(tlinkm(:,1)==id) //find (transitive) links with source "id"
		transls(id)=tlinkm(inds,2)
		jcondls(id)=jcm(inds)
	end
end
endfunction
