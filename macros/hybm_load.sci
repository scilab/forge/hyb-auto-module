// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//hybm_load(fn) - Load diagram from file with name fn (without path and extension)
//returns loaded diagram (scs_m structure)
function r=hybm_load(fn)
if argn(2)<>1 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"),"hybm_load",1))
end
if type(fn)<>10 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"hybm_load",1));
end
if size(fn,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string expected.\n"),"hybm_load",1));
end
[h,err]=mopen(TMPDIR+filesep()+fn+".xcos","r")
if err==0 then
	mclose(h)
	//importXcosDiagram(TMPDIR+filesep()+fn+".xcos") //this overwrites .h5 file
	//We cannot use importXcosDiagram until H5Read file lock is not fixed
	sv=hybm_save_name(fn+"_imptmp",".h5")
	xcosDiagramToHDF5(TMPDIR+filesep()+fn+".xcos",TMPDIR+filesep()+sv+".h5",%t)
	import_from_hdf5(TMPDIR+filesep()+sv+".h5")
	mdelete(TMPDIR+filesep()+sv+".h5")
	//For some reason xcosDiagramToHDF5 does not store doc field
	load(TMPDIR+filesep()+fn+".dat","propsdoc")
	scs_m.props.doc=propsdoc
//call to writeXcosDiagram without arguments checks whether we can write .xcos files
elseif ~writeXcosDiagram() then
	if ~import_from_hdf5(TMPDIR+filesep()+fn+".h5") then
		error("Can not open "+TMPDIR+filesep()+fn+".h5")
	end
end
r=scs_m
endfunction
