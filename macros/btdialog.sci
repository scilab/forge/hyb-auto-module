// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//btdialog(titl, txt, items, handler, arg) - display a dialog with buttons
//titl - dialog title (single string)
//txt - dialog text (single string)
//items - button captions (row or column vector of strings)
//handler - name of Scilab function, which handles button clicks: handler(but_number, arg)
//arg - static extra argument, passed to handler
//darg - starting value of dynamic (changes between calls) extra argument for handler
//tag - optional window tag (single string)
function btdialog(titl, txt, items, handler, arg, darg, tag)
if argn(2)==1 | argn(2)==2 | argn(2)>7 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d, or %d to %d expected.\n"),"btdialog",0,3,7))
end
if argn(2)<1 then
	titl="Options"
	txt="Select option"
	items=["Button1","Button2","Button3","Button 4"]
else
	if type(titl)<>10 then
		error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"btdialog",1));
	end
	if size(titl,"*")<>1 then
		error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string expected.\n"),"btdialog",1));
	end
	if type(txt)<>10 then
		error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"btdialog",2));
	end
	if size(txt,"*")<>1 then
		error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string expected.\n"),"btdialog",2));
	end
	if type(items)<>10 then
		error(msprintf(gettext("%s: Wrong type for input argument #%d: String array expected.\n"),"btdialog",3));
	end
	if size(items,1)<>1 & size(items,2)<>1 then
		error(msprintf(gettext("%s: Wrong size for input argument #%d: String array expected.\n"),"btdialog",3));
	end
end
if argn(2)<4 then
	handler="bt_test_handler"
else
	if type(handler)<>10 then
		error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"btdialog",4));
	end
	if size(handler,"*")<>1 then
		error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string expected.\n"),"btdialog",4));
	end
end
if argn(2)<5 then arg=[], end
if argn(2)<6 then darg=[], end
if argn(2)<7 then
	tag="_btdialog_"
else
	if type(tag)<>10 then
		error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"btdialog",7));
	end
	if size(tag,"*")<>1 then
		error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string expected.\n"),"btdialog",7));
	end
end
bcnt=size(items,"*");
txth=20; bh=50; bw=200;
toth=txth+50*bcnt;
f=figure("Figure_name",titl,..
		"Tag",tag,..
		"Position",[100 100 bw toth]);
delmenu(f.figure_id, gettext("&File"));
delmenu(f.figure_id, gettext("&Tools"));
delmenu(f.figure_id, gettext("&Edit"));
delmenu(f.figure_id, gettext("&?"));
toolbar(f.figure_id, "off");
f.visible="off";
f.axes_size=[bw toth];
uicontrol(f,"Style", "text",..
		"Position", [10 toth-txth bw txth],..
		"String", txt)
for i=1:size(items,"*")
	uicontrol(f,"Style", "pushbutton",..
			"Position", [0 toth-txth-bh*i bw bh],..
			"String", items(i),..
			"callback", handler+"("+string(i)+","+sci2exp(arg)+")");
end
dat=list([],darg)
set(f,"userdata",dat)
endfunction

//bt_test_handler(but_num, arg) - sample button click handler
//but_num - number of button
//arg - static argument, which was passed to btdialog
function bt_test_handler(but_num, arg)
messagebox("Button #"+string(but_num)+" pressed","Test handler")
endfunction
