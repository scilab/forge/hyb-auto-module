// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//Interfacing function of main hybrid automaton block
function [x,y,typ]=HAS_f(job, arg1, arg2)
x=[]; y=[]; typ=[];
select job
case "plot" then
	standard_draw(arg1)
case "getinputs" then
	[x,y,typ]=standard_inputs(arg1)
case "getoutputs" then
	[x,y,typ]=standard_outputs(arg1)
case "getorigin" then
	[x,y]=standard_origin(arg1)
case "set" then
	x=arg1; uid=arg1.doc(1);
	model=arg1.model; graphics=arg1.graphics; label=graphics.exprs
	if size(label,"*")==14 then label(9)=[], end //compatiblity
	if btdialog_getsig(uid) then
		//get dynamic arguments: state diagram name, block diagram name and
		//a boolean - whether state diagram was opened by user
		darg=btdialog_getdarg()
		winId=waitbar("Loading state diagram...")
		scs_st=hybm_load(darg(1)) //load state diagram
		waitbar(0.25,winId)
		stsz=evstr(scs_st.props.doc(2))
		initst=evstr(scs_st.props.doc(3))
		if size(scs_st.props.doc)>=4
			insz=evstr(scs_st.props.doc(4))
		else
			insz=0
		end
		//force re-transformation if user changed input control vector size
		if (model.in==[] & insz>0) | (model.in<>[] & model.in<>insz) then
			darg(3)=%t
		end
		if insz>0 then  //update input size
			model.in=insz
		else
			model.in=[]
		end
		model.out=[stsz;1] //update state size
		//save state diagram changes regardless of their validity
		model.opar(1)=scs_st
		if darg(3) then //diagram was modified, re-transform it
			waitbar("Transforming diagram...",winId)
			[scs_bk,ierr]=hybm_transf_diagr(scs_st) //transform state to block diagram
			waitbar(0.50,winId)
			if ierr==0 then	//OK
				darg(3)=%f //clear modification flag
			else //on transformation error, clear block diagram
				scs_emp=scicos_diagram(version=get_scicos_version())
				scs_emp.props.title="Hybrid automaton state diagram"
				scs_emp.props.doc=list("1",sci2exp(stsz),sci2exp(initst),sci2exp(insz))
				scs_bk=hybm_transf_diagr(scs_emp)
			end
		else //update state size and initial condition only (without re-transformation)
			scs_bk=hybm_modif_diagr(model.rpar,stsz,initst)
			darg(3)=%f
		end
		model.rpar=scs_bk //save block diagram in model
		darg(2)=hybm_save_name(darg(2))
		waitbar("Saving block diagram...",winId)
		hybm_save(darg(2),scs_bk) //export block diagram for viewing by user
		waitbar(0.75,winId)
		darg(1)=hybm_save_name(darg(1))
		waitbar("Saving state diagram...",winId)
		hybm_save(darg(1),scs_st) //update state diagram in file
		waitbar(1.0,winId)
		//Save diagram modification flag in opar so it can be restored if user closes HAS dialog
		model.opar(2)=darg(3)
		x.model=model //save sizes and possibly diagrams in super-block
		btdialog_setdarg(darg)
		winclose(winId)
	elseif ~btdialog_isopened() then
		winId=waitbar("Preparing diagrams...")
		tmpst=hybm_save_name("ha_st")
		hybm_save(tmpst,model.opar(1)) //dump state diagram to temporary file
		waitbar(0.50,winId)
		tmpbk=hybm_save_name("ha_bk")
		hybm_save(tmpbk,model.rpar) //dump block diagram to temporary file
		waitbar(1.0,winId)
		winclose(winId)
		if size(model.opar)>=2
			dgrmod=model.opar(2) //restore diagram modification flag if it is present
		else
			dgrmod=%f
		end
		btdialog("Options:"+uid,"Hybrid automaton super-block options",..
			["1. Set state diagram parameters";..
			"2. Edit/View state diagram";..
			"3. Update and transform state diagram";..
			"4. View resulting block diagram"],..
			"bt_has_handler",uid,list(tmpst,tmpbk,dgrmod))
	end
	needcompile=resume(needcompile)
case "define" then
	scs_st=scicos_diagram(version=get_scicos_version())
	scs_st.props.title="Hybrid automaton state diagram"
	scs_st.props.doc=list("1","1","0") //default state size and initial state
	scs_bk=hybm_transf_diagr(scs_st)
	model=scicos_model()
	model.sim="csuper"
	model.in=[]
	model.out=[1;1]
	model.opar=list(scs_st)
	model.rpar=scs_bk
	model.dep_ut=[%f %f]
	label=["_custom_superblock_"; "HAS_f"]
	gr_i=["xstringb(orig(1),orig(2),""HybAut"",sz(1),sz(2),""fill"");"]
	x=standard_define([3 2],model,label,gr_i)
	x.gui="DSUPER" //masked superblock
end
endfunction
