// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//Button click handler for HAS_f block GUI
//but_num - button number
//uid - string ID of a HAS_f block
function bt_has_handler(but_num, uid)
darg=btdialog_getdarg()
tmpst=darg(1); tmpbk=darg(2);
select but_num
case 1 then	//edit hybrid automaton parameters
	scs_st=hybm_load(tmpst)
	if size(scs_st.props.doc)>=4 then
		insz_str=scs_st.props.doc(4)
	else
		insz_str="0"
	end
	label=[scs_st.props.doc(2);scs_st.props.doc(3);insz_str]
	while %t do
		[ok,stsz,initst,insz,label]=..
		scicos_getvalue("Set hybrid automaton parameters",..
			["State size";"Initial state (vector)";"Input size"],..
			list("vec",1,"vec",-1,"vec",1),..
			label);
		if ~ok then break, end
		stsz=int(stsz)
		if stsz<1 then
			messagebox("State size should be >=1","modal")
			continue
		end
		insz=int(insz)
		if insz<0 then
			messagebox("Input size should be >=0","modal")
			continue
		end
		if size(initst,1)<>stsz then
			messagebox("Invalid size of initial state vector","modal")
			continue
		end
		scs_st.props.doc(2)=sci2exp(stsz)
		scs_st.props.doc(3)=sci2exp(initst)
		if insz>0 | size(scs_st.props.doc)>=4 then
			scs_st.props.doc(4)=sci2exp(insz)
		end
		darg(1)=hybm_save_name(tmpst)
		hybm_save(darg(1),scs_st)
		btdialog_setdarg(darg)
		break
	end
case 2 then //edit state diagram
	darg(3)=%t
	btdialog_setdarg(darg)
	hybm_open(tmpst,%t)
case 3 then //update state and block diagrams
	btdialog_setsig(uid)
	//If the function which allows to open block settings is not available, a user still
	//can update a block diagram by means of double clicking hybrid automaton block
	if execstr("hybm_xcosOpenBlock(uid)","errcatch")<>0 then
		messagebox(["Some functions required for this command are not available";...
		"To produce a block diagram, please double click on hybrid automaton block in the diagram"],"modal")
	end
case 4 then //open block diagram
	hybm_open(tmpbk,%f)
end
endfunction
