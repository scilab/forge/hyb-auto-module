// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//Function s1=hybm_prepexpr(s,n,m) converts expression from format acceptable by
//CSB/DSB/TGB blocks (variables u1,u2,..u_n for states and v1,v2,..v_m for input controls)
//to format acceptable by EXPRESSION block (all variables should have names u1,u2,..u_n+m)
//Parameters:
//s - expression string
//n - number of ui variables
//m - number of vi variables
//For example, hybm_prepexpr("u1+v1-2",2,2) == "u1+u3-2"
function s1=hybm_prepexpr(s,n,m)
if argn(2)<>3 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"),"hybm_prepexpr",2))
end
if type(s)<>10 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"hybm_prepexpr",1));
end
if size(s,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string expected.\n"),"hybm_prepexpr",1));
end
if type(n)<>1 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Number expected.\n"),"hybm_prepexpr",2));
end
if size(n,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Number expected.\n"),"hybm_prepexpr",2));
end
if type(m)<>1 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Number expected.\n"),"hybm_prepexpr",3));
end
if size(m,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Number expected.\n"),"hybm_prepexpr",3));
end
n=int(n)
m=int(m)
if m==0 then
	s1=s
	return
end
s0=" "+s+" "
//Rename variable v_i to u_(i+n) for all i
//A standard function strsubst does not support regexp substitutions (supports regexp
//matching only), so we have to rename variables manually
s1=""
k=0 //current position in output string (s1)
mat=tokenpos(s0,"v") //find positions which delimit names "v"
for i=1:size(mat,1)-1 do
	s1=s1+part(s0,(k+1):mat(i,2)) //copy part of expression up to current "v"
	k=mat(i,2) //advance k
	prevchr=part(s0,mat(i,2)) //character before "v"
	if isalphanum(prevchr) | prevchr=='_' then continue, end //it should be non-word char
	nextok=part(s0,(mat(i,2)+1):mat(i+1,2)) //part of expression after "v" (including "v")
	[a,b]=regexp(nextok,"/v\d+\W/") //it should be a number followed by non-word character
	if a<>1 then continue, end
	//positions 1..b-1 contain "v" and variable index
	//"v" should be replaced with "u" and variable index should be updated
	varidx=int(strtod(part(nextok,2:(b-1))))
	if varidx>0 & varidx<=m
		newidx=string(varidx+n)
		s1=s1+"u"+newidx
		k=k+b-1
	end
end
s1=s1+part(s0,(k+1):length(s0)) //copy rest of string
s1=stripblanks(s1)
endfunction
