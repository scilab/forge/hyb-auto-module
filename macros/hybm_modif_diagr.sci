// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//hybm_modif_diagr(scs_bk,stsz,initst) - update block diagram parameters without transformation
//scs_bk - source block diagram
//stsz, initst - block diagram parameters (number and column vector)
//scs - updated block diagram
function scs=hybm_modif_diagr(scs_bk, stsz, initst)
if argn(2)<>3 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"),"hybm_modif_diagr",1))
end
if type(scs_bk)<>17 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: scs_m structure expected.\n"),"hybm_modif_diagr",1));
end
if type(stsz)<>1 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Number expected.\n"),"hybm_modif_diagr",2));
end
if size(stsz,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Number expected.\n"),"hybm_modif_diagr",2));
end
if type(initst)<>1 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Real column vector expected.\n"),"hybm_modif_diagr",3));
end
if size(initst(1,:),"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Real column vector expected.\n"),"hybm_modif_diagr",3));
end
scs=scs_bk
ok=%f
for i=1:size(scs.objs) //find AUTOMAT block and update its parameters
	blk=scs.objs(i)
	if typeof(blk)=="Block" then
		if blk.gui=="AUTOMAT" | blk.gui=="AUTOMAT2" then
			blk=modifAutomat(blk,stsz,initst)
			ok=%t
		end
	end
	if ok then
		scs.objs(i)=blk
		break
	end
end
if ~ok & size(scs.objs)>0 then //AUTOMAT block not found
	messagebox("Block diagram parameters can not be updated: AUTOMAT not found")
end
endfunction

//function modifAutomat sets state size (stsz) and initial state (initst)
//parameters in AUTOMAT(2) block
function blk_=modifAutomat(blk, stsz, initst)
blk_=blk
N=blk.model.ipar(1)
M=stsz
y0=initst
init=blk.model.ipar(2)
XP=ones(N,M)
ipar=[N;init;M;matrix(XP',N*M,1)]
exprs=[string(N);string(init);string(M);sci2exp(y0);sci2exp(XP(1,:))]
ins=ones(N,1)
outs=[2;2*M]
nzcross=0
for i=1:N do
	tran_vec=evstr(blk.graphics.exprs(5+i))
	ipar=[ipar;tran_vec]
	exprs=[exprs;sci2exp(tran_vec)]
	leni=length(tran_vec)
	ins(i,1)=2*M+leni
	if nzcross<leni then
		nzcross=leni
	end
end
[model,graphics,ok]=check_io(blk.model,blk.graphics,ins,outs,[],[1])
model.nzcross=nzcross
model.state=ones(2*M,1)
model.ipar=ipar
model.rpar=y0
graphics.exprs=exprs
blk_.model=model
blk_.graphics=graphics
endfunction
