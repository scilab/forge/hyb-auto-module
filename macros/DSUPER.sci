// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//Interfacing function of masked superblock, required for hybrid automaton block
function [x,y,typ]=DSUPER(job, arg1, arg2)
x=[]; y=[]; typ=[];
grp=arg1.graphics
if size(grp.exprs,"*")>=2
	if grp.exprs(1) == "_custom_superblock_"
		execstr("[x,y,typ] ="+grp.exprs(2)+"(job,arg1,arg2)");
		return
	end
end
[x,y,typ] = Misclib.DSUPER(job,arg1,arg2)
endfunction
