// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//Function hybm_gen_maindgr(st_eqls,transls,jcondls,init,y0,insz) generates intermediate
//representation (in format, accepted by hybm_mkdgr) of main block diagram from hybrid
//automaton"s description - state equations (st_eqls), mode transitions (transls), jump
//conditions (jcondls), initial mode and state (init,y0). Main block diagram contains
// AUTOMAT block and superblocks (subsystems) for each mode. It looks like follows:
//  |->-AUTOMAT--->---extractor-->--out1 (current state)
//  |          \-->---extractor-->--out2 (current mode)
//  |          \-->-|
//  |-<-subsystem-<-|
//  |-<-subsystem-<-|
//  |     ...       |
//Suppose that hybrid automaton has N>=1 modes and (continuous time) state (u1,u2,...,uM)
//(here u1,u2,.. are scalars).
//1) st_eqls - description of state equations list(st_eqs_1,st_eqs_2,...,st_eqs_N), where
//st_eqs_i=list(["<eq_i1>";..;"<eq_iM>"],"<state_kind>",<optional parameters>,...), i=1..N.
//Here <eq_ij> are expressions in state variables u1,u2,...,uM and control variables
//v1,v2,.. (insz variables)
//"<state_kind>" is "c" for continuous state, "d" for discrete state
//optional parameters may include time delta for discrete states, etc. 
//In the i-th mode, automaton"s state changes according to ODEs: duj/dt=<eq_ij>, j=1..M
//2) transls - description of transitions list(w_1,w_2,...,w_N), where w_i - column vector
//of modes (integers), to which automaton can switch from i-th mode
//3) jcondls - description of jump conditions list(v_1,v_2,...,v_N), where
//v_i - column vector of string expressions in state variables u1,u2,.., one for each
//transition from i-th automaton's mode
//4) init - initial mode
//5) y0 - initial state (column vector of size M)
//6) insz - (optional) size of input control vector (default 0)
//Now there is a minor problem with visual appearance of diagram (not flipped blocks),
//produced by this function due to open bug #7257
function [blockls,connls]=hybm_gen_maindgr(st_eqls, transls, jcondls, init, y0, insz)
if argn(2)<>5 & argn(2)<>6 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"),"hybm_gen_maindgr",6))
end
if argn(2)<6 then
	insz=0
end
if type(st_eqls)<>15 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: List expected.\n"),"hybm_gen_maindgr",1));
end
if type(transls)<>15 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: List expected.\n"),"hybm_gen_maindgr",2));
end
if type(jcondls)<>15 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: List expected.\n"),"hybm_gen_maindgr",3));
end
if type(init)<>1 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Real number expected.\n"),"hybm_gen_maindgr",4));
end
if size(init,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Real number expected.\n"),"hybm_gen_maindgr",4));
end
if type(y0)<>1 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Real column vector expected.\n"),"hybm_gen_maindgr",5));
end
if size(y0(1,:),"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Real column vector expected.\n"),"hybm_gen_maindgr",5));
end
if type(insz)<>1 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Number expected.\n"),"hybm_gen_maindgr",6));
end
if size(insz,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Number expected.\n"),"hybm_gen_maindgr",6));
end
insz=int(insz)
if insz==0 invec=[], else invec=insz, end
N=size(st_eqls) //number of modes
M=size(st_eqls(1)(1),1) //state size
//add outputs and inputs
blockls=list(list("out1",[500,60*N],"Out",1),list("out2",[500,60*(N+1)],"Out",2))
if insz>0
	blockls($+1)=list("in1",[100,60*(N)],"In",1)
end
has_dsb=%f //whether DSB blocks are present
for i=1:N do
	if st_eqls(i)(2)=="d" then
		has_dsb=%t
		break
	end
end
for i=1:N do //add subsystems
	if st_eqls(i)(2)=="c" then //continuous-time
		//generate subsystem
		[blockls1,connls1]=hybm_gen_subsys(st_eqls(i)(1),jcondls(i),insz)
		scs=hybm_mkdgr(blockls1,connls1)
		//add subsystem (flip block)
		blockls($+1)=list("subsys"+sci2exp(i),[-250,60*(N-i+1)],"Super",[1;invec],1,scs)
	elseif st_eqls(i)(2)=="d" then //discrete-time
		[blockls1,connls1]=hybm_gen_dsubsys(st_eqls(i)(1),jcondls(i),st_eqls(i)(3),i,insz)
		scs=hybm_mkdgr(blockls1,connls1)
		blockls($+1)=list("subsys"+sci2exp(i),[-250,60*(N-i+1)],"Super",[2*M;2;invec],1,scs)
		//add loop transition for discrete-time state updating
		transls(i)=[-transls(i);100000]
	else
		error("Unknown state kind: "+st_eqls(i)(2))
	end
	blockls($+1)=list("spl"+sci2exp(i),[350,60*(N)],"Split") //add input splitter
end
//add AUTOMAT block if no there are not DSB blocks and AUTOMAT2 if DSB blocks are present
if ~has_dsb then
	blockls($+1)=list("aut",[250,60*(N+1)],"Automat",N,M,transls,init,y0)
else
	blockls($+1)=list("aut",[250,60*(N+1)],"Automat",N,M,transls,init,y0,2)
end
blockls($+1)=list("extr",[400,60*N],"Extr",[1:M]) //add extractor for state
blockls($+1)=list("extrm",[400,60*(N+1)],"Extr",[1]) //add extractor for mode
connls=list()
//name of splitter for 2-nd input of discrete subsystem (changes during "for" cycle)
prev_dspl=""
prev_ispl="" //splitter for input controls
for i=1:N do //connect subsystems to automat
	if i>1 then
		connls($+1)=list("spl"+sci2exp(i-1),1,"spl"+sci2exp(i),1)
	else
		connls($+1)=list("aut",2,"spl1",1)
	end	
	connls($+1)=list("spl"+sci2exp(i),2,"subsys"+sci2exp(i),1)
	connls($+1)=list("subsys"+sci2exp(i),1,"aut",i)
	//discrete-time subsystems require second input (connected with AUTOMAT output 1)
	if st_eqls(i)(2)=="d" then
		blockls($+1)=list("dspl"+sci2exp(i),[350,60*(N+1)],"Split")
		if prev_dspl<>"" then
			connls($+1)=list(prev_dspl,1,"dspl"+sci2exp(i),1)
		else
			connls($+1)=list("aut",1,"dspl"+sci2exp(i),1)
		end
		connls($+1)=list("dspl"+sci2exp(i),2,"subsys"+sci2exp(i),2)
		prev_dspl="dspl"+sci2exp(i)
	end
	//connect input controls (if they are present)
	if insz>0 then
		blockls($+1)=list("ispl"+sci2exp(i),[150,60*(N)],"Split") //input control slitter
		if prev_ispl<>"" then
			connls($+1)=list(prev_ispl,1,"ispl"+sci2exp(i),1)
		else
			connls($+1)=list("in1",1,"ispl"+sci2exp(i),1)
		end
		//control port number depends on state type
		if st_eqls(i)(2)=="c" then portno=2, else portno=3, end
		connls($+1)=list("ispl"+sci2exp(i),2,"subsys"+sci2exp(i),portno)
		prev_ispl="ispl"+sci2exp(i)
	end
end
connls($+1)=list("spl"+sci2exp(N),1,"extr",1) //a copy of state->extractor
connls($+1)=list("extr",1,"out1",1) //extractor->output
//connect mode extractor
if prev_dspl<>"" then //there were discrete-time subsystems
	connls($+1)=list(prev_dspl,1,"extrm",1)
else
	connls($+1)=list("aut",1,"extrm",1)
end
connls($+1)=list("extrm",1,"out2",1) //mode extractor->output
endfunction
