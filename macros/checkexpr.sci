// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


//Function checkexpr(s,n,m) checks validity of expression in variables u1,...,un,v1,..,vm
//parameter m is optional (default 0)
//On error returns %f
function b=checkexpr(s,n,m)
if argn(2)<>2 & argn(2)<>3 then
	error(msprintf(gettext("%s: Wrong number of input arguments: %d expected.\n"),"checkexpr",3))
end
if argn(2)<3 then
	m=0
end
if type(s)<>10 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Single string expected.\n"),"checkexpr",1));
end
if size(s,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Single string expected.\n"),"checkexpr",1));
end
if type(n)<>1 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Number expected.\n"),"checkexpr",2));
end
if size(n,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Number expected.\n"),"checkexpr",2));
end
if type(m)<>1 then
	error(msprintf(gettext("%s: Wrong type for input argument #%d: Number expected.\n"),"checkexpr",3));
end
if size(m,"*")<>1 then
	error(msprintf(gettext("%s: Wrong size for input argument #%d: Number expected.\n"),"checkexpr",3));
end
ok=%f
%head="%foo("
if n>0 then
	for %jji=1:n-1,%head=%head+"u"+string(%jji)+",",end
	%head=%head+"u"+string(n)
end
if m>0 then
	%head=%head+","
	for %jji=1:m-1,%head=%head+"v"+string(%jji)+",",end
	%head=%head+"v"+string(m)
end
%head=%head+")"
ok=execstr("deff(%head,s)","errcatch")==0
if ok then
	[ok,%ok1,ipar,rpar,%nz]=compiler_expression(%foo)
	if ~%ok1 then
		ok=%f
	end
end
b=ok
endfunction
