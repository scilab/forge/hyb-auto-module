// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


mode(-1);
lines(0);

TOOLBOX_NAME  = "hyb_auto_module";
TOOLBOX_TITLE = "Hybrid Automata Module for Scilab/Xcos";
toolbox_dir   = get_absolute_file_path("builder.sce");

// Check Scilab's version
// =============================================================================

try
	v = getversion("scilab");
catch
	error(gettext("Scilab 5.3 or more is required."));
end

if v(2) < 3 then
	// new API in scilab 5.3
	error(gettext("Scilab 5.3 or more is required."));
end

// Check development_tools module avaibility
// =============================================================================

if ~with_module("development_tools") then
  error(msprintf(gettext("%s module not installed."),"development_tools"));
end

// Action
// =============================================================================

tbx_builder_macros(toolbox_dir);
tbx_builder_src(toolbox_dir);
tbx_builder_gateway(toolbox_dir);
tbx_builder_help(toolbox_dir);
tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

// Use SEP 45
imgdir = toolbox_dir + "images/";
h5dir  = toolbox_dir + "images/h5/";
xpal   = xcosPal("Hybrid automata module");
blks   = list("HAS_f", "CSB_f", "DSB_f", "TGB_f");
blklib = lib(toolbox_dir + "macros");
loadScicosLibs();

// Add blocks to the palette
for blkname = blks
	scs_m = evstr(blkname+"(""define"")");
	export_to_hdf5(h5dir+blkname+".h5", "scs_m");
	clear scs_m;
	blkstyle = struct();
	blkstyle.image = "file:"+imgdir+blkname+".svg";
	xpal = xcosPalAddBlock(xpal, h5dir+blkname+".h5", imgdir+blkname+".png", blkstyle);
end

// Save the palette
xcosPalExport(xpal, toolbox_dir + "hyb_auto_module.xpal");

// Clean variables
// =============================================================================

clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE;
clear imgdir h5dir xpal blks blklib;
