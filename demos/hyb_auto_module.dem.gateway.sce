// ====================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
demopath = get_absolute_file_path("hyb_auto_module.dem.gateway.sce");

subdemolist = ["Two water tanks", "watertankha.dem.sce";..
				"Thermostat", "thermostat.dem.sce";..
				"Continuous and Discrete", "mixed.dem.sce";..
				"Controlled automaton", "controlha.dem.sce";..
				"Large automaton", "largeaut.dem.sce"];

subdemolist(:,2) = demopath + subdemolist(:,2);
// ====================================================================
