/*
* Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
* Copyright (C) 2010 - DIGITEO - Ievgen IVANOV
*
* This file must be used under the terms of the CeCILL.
* This source file is licensed as described in the file COPYING, which
* you should have received as part of this distribution.  The terms
* are also available at
* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
*
*/
/*--------------------------------------------------------------------------*/
#include "XcosControl.hxx"

extern "C"
{
#include "stack-c.h"
#include "api_common.h"
#include "api_string.h"
#include "localization.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "freeArrayOfString.h"
#include "getScilabJavaVM.h"
}
/*--------------------------------------------------------------------------*/
using namespace org_scilab_modules_hyb_auto_module;
/*--------------------------------------------------------------------------*/
extern "C" int sci_xcosOpenBlock(char *fname,unsigned long fname_len)
{
	SciErr sciErr;


	CheckRhs(1,1);
	CheckLhs(0,1);

	int m1 = 0, n1 = 0;
	int *piAddressVarOne = NULL;

	char *pStVarOne = NULL;
	int lenStVarOne = 0;

	/** READ UID **/
	sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	/* get dimensions */
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne, &m1, &n1, NULL, NULL);
	if(sciErr.iErr)
	{
		printError(&sciErr, 0);
		return 0;
	}

	if (m1 * n1 != 1) {
		Scierror(999,_("%s: Wrong size for input argument #%d: A string expected.\n"),fname,1);
		return 0;
	} 

	/* get lengths */
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne, &m1, &n1, &lenStVarOne, NULL);
	if(sciErr.iErr)
	{
		if (lenStVarOne) { FREE(lenStVarOne); lenStVarOne = NULL;}
		printError(&sciErr, 0);
		return 0;
	}

	pStVarOne = (char*)MALLOC(sizeof(char*) * (lenStVarOne + 1));

	/* get strings */
	sciErr = getMatrixOfString(pvApiCtx, piAddressVarOne, &m1, &n1, &lenStVarOne, &pStVarOne);
	if(sciErr.iErr)
	{
		FREE(pStVarOne);
		printError(&sciErr, 0);
		return 0;
	}

	XcosControl::xcosOpenBlockSettings(getScilabJavaVM(), pStVarOne);

	LhsVar(1) = 0;
	PutLhsVar();

	FREE(pStVarOne);
	return 0;
}
/*--------------------------------------------------------------------------*/
