// Copyright (C) 2010 - Ievgen IVANOV
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

src_c_path = get_absolute_file_path("builder_c.sce");
CFLAGS = "-I" + SCI + "/modules/scicos/includes" + " -I" + SCI + "/modules/scicos_blocks/includes";
LDFLAGS = "";
if (getos()<>"Windows") then
	if ~isdir(SCI+"/share") then
		//Development version of Scilab on non-Windows platform
		CFLAGS = CFLAGS + " -I" + SCI + "/../../modules/scicos_blocks/includes";
	end
else
	if findmsvccompiler() <> "unknown" & haveacompiler() then
		LDFLAGS = LDFLAGS + " """ + SCI + "/bin/scicos.lib""";
		LDFLAGS = LDFLAGS + " """ + SCI + "/bin/scicos_f.lib""";
	end
end

tbx_build_src(["automat2"], ["automat2.c"], "c", src_c_path, "", LDFLAGS, CFLAGS);

clear tbx_build_src;
clear src_c_path;
clear CFLAGS;
