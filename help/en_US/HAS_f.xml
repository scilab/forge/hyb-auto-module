<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="HAS_f">
  <refnamediv>
    <refname>HAS_f</refname>
    <refpurpose>Hybrid automaton super block</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../images/HAS_f.png" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_HAS_f">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="HAS_f">Hybird automaton super block</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_HAS_f">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_HAS_f">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Defaultproperties_HAS_f">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_HAS_f">Interfacing function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Authors_HAS_f">Authors</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_HAS_f">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="hyb_auto_module_pal">Demonstrations blocks palette</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_HAS_f">
    <title>Description</title>
    <para>
This block allows to specify a hybrid automaton in the form of state diagram.
First output contains current phase space state of hybrid automaton, second output
contains currect discreate state (mode) of hybrid automaton.
</para>
<inlinemediaobject><imageobject><imagedata fileref="../../images/HAS_f_img1.png" align="center" valign="middle"/></imageobject></inlinemediaobject>
    <para>
The block internally stores a hybrid automaton's state diagram and a corresponding
block diagram, which is used during simulation. To specify a hybrid automaton you
have to put a block on a diagram, open block's dialog (double click the block) and
perform the following steps:
</para>
    <para>
1. Click button 1 ("Set state diagram parameters") to set dimension of phase space
(field "State size"), initial continuous state (a column vector, field "Initial state
(vector)") and input control size (field "Input size").
</para>
<inlinemediaobject><imageobject><imagedata fileref="../../images/HAS_f_img2.png" align="center" valign="middle"/></imageobject></inlinemediaobject>
    <para>
2. Click button 2 ("Edit/View state diagram"). Block opens a new diagram window.
In this window you can draw a hybrid automaton's state diagram with the help of
CSB_f (continuous state block), DSB_f (discrete state block) and TGB_f (transition
guard block). State diagram should not contain blocks of any other kind and should
contain at least one CSB_f or DSB_f block. CSB_f/TGB_f blocks represent hybrid
automaton modes. They should be connected only through TGB_f blocks. You have to
set parameters of each block (differential/difference equations, initial flags for
CSB_f/DSB_f blocks and equations for TGB blocks). Exactly one CSB_f or DSB_f block
should be initial. You have to save and close state diagram after editing. Note
that HAS_f block internally stores a copy state diagram. This copy is not
automatically updated when you save a diagram. To update it, you have to perform
the next (third) step. If you save a state diagram and immediately click button 2,
you will loose changes.
</para>
<inlinemediaobject><imageobject><imagedata fileref="../../images/HAS_f_img3.png" align="center" valign="middle"/></imageobject></inlinemediaobject>
    <para>
3. Click button 3 ("Update and transform state diagram"). This action will update
a state diagram stored in HAS block and prepare an internal block diagram. If a
state diagram is incorrect, an internal copy of state diagram will be updated and
an error message will be displayed, but internal block diagram will not be
updated. In this case you should click button 2, make corrections and click
button 3 once more.
</para>
    <para>
4. Optionally you can click button 4 ("View resulting block diagram") to view an
internal block diagram, generated from state diagram. This diagram is used during
simulation of an external diagram which contains HAS_f block.
</para>
<inlinemediaobject><imageobject><imagedata fileref="../../images/HAS_f_img4.png" align="center" valign="middle"/></imageobject></inlinemediaobject>
    <para>
5. Close HAS block's dialog. Now you can connect HAS_f block's outputs to other
blocks (for example Scope) and start simulation. Also you can convert HAS_f block
into regular Xcos superblock. To do this choose an item "Superblock
mask"->"Remove" from HAS_f block context menu. Note that this operation preserves
only internal block diagram stored in HAS_f block (state diagram can no more be
edited).
</para>
  </refsection>
  <refsection id="Defaultproperties_HAS_f">
    <title>Default properties</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">always active:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">zero-crossing:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> no</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">regular outputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [1,1] / type 1</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 2 : size [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation inputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation outputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">continuous-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">object discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">name of computational function:</emphasis>
          <emphasis role="italic">csuper</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Interfacingfunction_HAS_f">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para> hyb-auto-module/macros/HAS_f.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Authors_HAS_f">
    <title>Authors</title>
    <para><emphasis role="bold">Ievgen Ivanov</emphasis></para>
  </refsection>
</refentry>
