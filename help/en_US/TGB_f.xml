<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) DIGITEO - 2010 - Ievgen IVANOV <ivanov.eugen@gmail.com>
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="TGB_f">
  <refnamediv>
    <refname>TGB_f</refname>
    <refpurpose>Transition guard block</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../images/TGB_f.png" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_TGB_f">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="TGB_f">Transition guard block</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_TGB_f">Palette</xref>
            </para>
          </listitem>
		  <listitem>
            <para>
              <xref linkend="Description_TGB_f">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Dialogbox_TGB_f">Dialog box</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Defaultproperties_TGB_f">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_TGB_f">Interfacing function</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_TGB_f">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="hyb_auto_module_pal">Hybrid Automata Module palette</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_TGB_f">
    <title>Description</title>
    <para>
	This block represents a transition in hybrid automaton's state diagram. It
	should be used only in state diagrams specified with the help of HAS_f block.
	</para>
  </refsection>
  <refsection id="Dialogbox_TGB_f">
    <title>Dialog box</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../images/TGB_f_gui.png" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Transition of change of sign</emphasis>
        </para>
        <para> Value 0 means that transition becomes active when expression
		(defined below) changes sign from - to +. Value 1 means that transition
		becomes active when expression changes sign from + to -. 
		Note that if CSB_f block is connected to TGB_f, then transition occurs when the
		value of the expression crosses zero in the specified direction. If DSB_f block
		is connected to TGB_f, then transition occurs when expression becomes
		positive/negative for the first time.
		If "Input size" parameter of HAS_f block is non-zero, then you can use
		use input control variables v1,v2,... in expression.
		These variables represent current values on input port of HAS_f block.
		</para>
        <para> Properties : Type 'vec' of size 1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Expression</emphasis>
        </para>
        <para> String expression in variables u1,u2,...,uN which represent hybrid automaton's continuous state.</para>
        <para> Properties : Type 'str' of size 1. </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Defaultproperties_TGB_f">
    <title>Default properties</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">always active:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">zero-crossing:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> no</para>
      </listitem>
	  <listitem>
        <para>
          <emphasis role="bold">regular inputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">regular outputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation inputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation outputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">continuous-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">object discrete-time state:</emphasis> no</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Interfacingfunction_TGB_f">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para> hyb-auto-module/macros/TGB_f.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>  
</refentry>
